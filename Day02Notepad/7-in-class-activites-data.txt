PZ4 - Programming II
In-Class Activities - Class 7
:)

ICA-7-1
--------

7-1.1
What is a base case?

Answer: 



7-1.2
What is a recursive case?

Answer: 

7-1-3
Consider the following recursive method:

public static int mystery(int number) 			//Line 1
{
	if (number == 0) { //Line 2
		return number; 			//Line 3	
	} else { //Line 4
		return(number + mystery(number – 1)); 	//Line 5
	}
}

a. Identify the base case.

b. Identify the general case.

c. What valid values can be passed as parameters to the method mystery?

d. If mystery(0) is a valid call, what is its value? If it is not a valid call, explain why.

e. If mystery(5) is a valid call, what is its value? If not, explain why.

f. If mystery(-3) is a valid call, what is its value? If not, explain why.

Answer: 


ICA-7-2
--------
Consider the following recursive function:

public static void recFun(int u) {
	if (u == 1) {
		System.out.print("Stop! ");
	} else {
		System.out.print("Go ");
		recFun(u - 1);
	}
}

What is the output, if any, of the following statements?
a. recFun(7); 
b. recFun(3); 
c. recFun(-6);

Answer: 



ICA-7-3
--------
//Consider the following method:

public static int func(int x) {
	if (x == 0) {
		return 2;
		} else if (x == 1) {
			return 3;
			} else {
				return (func(x - 1) + func(x - 2));
		}
} // end method func	

What is the output of the following statements?
			a. System.out.println(func(0));
			b. System.out.println(func(1));
			c. System.out.println(func(2));
			d. System.out.println(func(5));
Answer:  



