
package day06sorting;

import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author artem
 */
public class Person implements Comparable<Person>{

    public Person(String name, int age, double heightMeters) {
        this.name = name;
        this.age = age;
        this.heightMeters = heightMeters;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getHeightMeters() {
        return heightMeters;
    }
    
    String name;
    int age;
    double heightMeters;

    @Override
    public String toString() {
        return String.format("Person{%s,%d,%.2f}", name, age, heightMeters);
    }

    @Override
    public int compareTo(Person o) {
        return this.age - o.age;
    }
   

//static final Comparator<Person> compareByNameLambda = (Person p1, Person p2) -> p1.name.compareTo(p2.name);
//static final Comparator<Person> compareByheightMetersLambda = (Person p1, Person p2) -> 
//        Double.toString(p1.heightMeters).compareTo(Double.toString(p2.heightMeters));
//static final Comparator<Person> compareByNameAgeLambda = (Person p1, Person p2) -> {
//    if(p1.name.compareTo(p2.name)!=0){
//    return p1.name.compareTo(p2.name);
//    }
//    return Integer.toString(p1.age).compareTo(Integer.toString(p2.age));
//};
//
//static final Comparator<Person> compareByNameAnonClass = new Comparator<>(){
//
//    @Override
//    public int compare(Person p1, Person p2) {
//        return p1.name.compareTo(p2.name);
//    }
//};
}      
