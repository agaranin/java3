
package day06sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author artem
 */
public class Day06Sorting {

    static ArrayList<Person> people = new ArrayList<>();
    
    public static void main(String[] args) {
        people.add(new Person("Adam", 65, 1.54));
        people.add(new Person("Adam", 45, 1.45));
        people.add(new Person("Adam", 23, 1.65));
        people.add(new Person("Elsa", 78, 1.98));
        people.add(new Person("Artem", 98, 1.23));
        people.add(new Person("Vitaly", 12, 1.32));
        System.out.println("Original order");
        System.out.println(Arrays.deepToString(people.toArray()));
        
        Collections.sort(people);
        System.out.println("Ordered by age (using Comparable)");
        System.out.println(Arrays.deepToString(people.toArray()));
        
//        Collections.sort(people, Person.compareByNameLambda);
//        System.out.println("Ordered by age (using Lambda)");
//        System.out.println(Arrays.deepToString(people.toArray()));
//        
//        Collections.sort(people, Person.compareByheightMetersLambda);
//        System.out.println("Ordered by heightMeters (using Lambda)");
//        System.out.println(Arrays.deepToString(people.toArray()));
//        
//        Collections.sort(people, Person.compareByNameAgeLambda);
//        System.out.println("Ordered by name and age (using Lambda)");
//        System.out.println(Arrays.deepToString(people.toArray()));
        Collections.sort(people, Comparator.comparingInt(Person::getAge));
        System.out.println("Ordered by age (using Referenced method)");
        System.out.println(Arrays.deepToString(people.toArray()));
        
        Collections.sort(people, Comparator.comparingDouble(Person::getHeightMeters));
        System.out.println("Ordered by Height Meters (using Referenced method)");
        System.out.println(Arrays.deepToString(people.toArray()));
        
        Collections.sort(people, Comparator.comparing(Person::getName).thenComparingInt(Person::getAge));
        System.out.println("Ordered by name and age (using Referenced method)");
        System.out.println(Arrays.deepToString(people.toArray()));
    }
    
}
