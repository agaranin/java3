package day10flights;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author artem
 */
public class Day10flights extends javax.swing.JFrame {

    DefaultListModel<Flight> modelFlightsList = new DefaultListModel<>();
    DefaultComboBoxModel<Flight.Type> modelTypeComboList = new DefaultComboBoxModel<>(Flight.Type.values());
    Database db;

    /**
     * Creates new form Day10flights
     */
    public Day10flights() {
        initComponents();
        try {
            db = new Database(); // throw SQLException
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to connect" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        loadFlightsFromDatabase();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Text documents (*.txt)", "txt"));
    }

    void loadFlightsFromDatabase() {
        try {
            ArrayList<Flight> list = db.getAllFlights(); // throw SQLException, InvalidDataException
            modelFlightsList.clear();
            modelFlightsList.addAll(list);
            lblStatus.setText("Total Flights: " + modelFlightsList.getSize());
        } catch (SQLException | InvalidDataException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to get all todos" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }     
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgAddEdit = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        dlgAddEdit_lblId = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        dlgAddEdit_tfDate = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        dlgAddEdit_comboType = new javax.swing.JComboBox<>();
        dlgAddEdit_sliderPassengers = new javax.swing.JSlider();
        jLabel6 = new javax.swing.JLabel();
        dlgAddEdit_lblPassengers = new javax.swing.JLabel();
        dlgAddEdit_btCancel = new javax.swing.JButton();
        dlgAddEdit_btSave = new javax.swing.JButton();
        dlgAddEdit_tfFromCode = new javax.swing.JTextField();
        dlgAddEdit_tfToCode = new javax.swing.JTextField();
        buttonGroup1 = new javax.swing.ButtonGroup();
        popupListFlights = new javax.swing.JPopupMenu();
        popupDelete = new javax.swing.JMenuItem();
        fileChooser = new javax.swing.JFileChooser();
        lblStatus = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstFlights = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miSave = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miExit = new javax.swing.JMenuItem();
        miAdd = new javax.swing.JMenu();

        dlgAddEdit.setModal(true);
        dlgAddEdit.setResizable(false);

        jLabel1.setText("id:");

        dlgAddEdit_lblId.setText("-");

        jLabel3.setText("Date:");

        dlgAddEdit_tfDate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd-MM-yyyy"))));

        jLabel4.setText("From code:");

        jLabel5.setText("To code:");

        dlgAddEdit_comboType.setModel(modelTypeComboList);

        dlgAddEdit_sliderPassengers.setMajorTickSpacing(50);
        dlgAddEdit_sliderPassengers.setMaximum(200);
        dlgAddEdit_sliderPassengers.setMinorTickSpacing(25);
        dlgAddEdit_sliderPassengers.setPaintLabels(true);
        dlgAddEdit_sliderPassengers.setPaintTicks(true);
        dlgAddEdit_sliderPassengers.setValue(0);
        dlgAddEdit_sliderPassengers.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                dlgAddEdit_sliderPassengersStateChanged(evt);
            }
        });

        jLabel6.setText("Passengers:");

        dlgAddEdit_lblPassengers.setText("37");

        dlgAddEdit_btCancel.setText("Cancel");
        dlgAddEdit_btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btCancelActionPerformed(evt);
            }
        });

        dlgAddEdit_btSave.setText("Save");
        dlgAddEdit_btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgAddEditLayout = new javax.swing.GroupLayout(dlgAddEdit.getContentPane());
        dlgAddEdit.getContentPane().setLayout(dlgAddEditLayout);
        dlgAddEditLayout.setHorizontalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addComponent(dlgAddEdit_sliderPassengers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addComponent(dlgAddEdit_btCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addComponent(dlgAddEdit_btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgAddEdit_lblPassengers, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dlgAddEdit_lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(114, 114, 114))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createSequentialGroup()
                                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel5))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(dlgAddEdit_tfFromCode)
                                            .addGroup(dlgAddEditLayout.createSequentialGroup()
                                                .addComponent(dlgAddEdit_tfDate, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(dlgAddEdit_tfToCode)))
                                    .addComponent(dlgAddEdit_comboType, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(16, 16, 16))))))
        );
        dlgAddEditLayout.setVerticalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(dlgAddEdit_lblId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dlgAddEdit_tfDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dlgAddEdit_tfFromCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dlgAddEdit_tfToCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dlgAddEdit_comboType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dlgAddEdit_sliderPassengers, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(dlgAddEdit_lblPassengers))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_btCancel)
                    .addComponent(dlgAddEdit_btSave))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        popupDelete.setText("Delete");
        popupDelete.setToolTipText("");
        popupDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popupDeleteActionPerformed(evt);
            }
        });
        popupListFlights.add(popupDelete);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        lblStatus.setText("jLabel1");
        lblStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        getContentPane().add(lblStatus, java.awt.BorderLayout.PAGE_END);

        lstFlights.setModel(modelFlightsList);
        lstFlights.setComponentPopupMenu(popupListFlights);
        lstFlights.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstFlightsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstFlights);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jMenu1.setText("File");

        miSave.setText("Save selected");
        miSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSaveActionPerformed(evt);
            }
        });
        jMenu1.add(miSave);
        jMenu1.add(jSeparator1);

        miExit.setText("Exit");
        miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExitActionPerformed(evt);
            }
        });
        jMenu1.add(miExit);

        jMenuBar1.add(jMenu1);

        miAdd.setText("Add");
        miAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                miAddMouseClicked(evt);
            }
        });
        jMenuBar1.add(miAdd);

        setJMenuBar(jMenuBar1);

        setSize(new java.awt.Dimension(425, 356));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cleanInputs() {
        dlgAddEdit_lblId.setText("-");
        dlgAddEdit_tfDate.setText("");
        dlgAddEdit_tfFromCode.setText("");
        dlgAddEdit_tfToCode.setText("");
        dlgAddEdit_sliderPassengers.setValue(0);
        dlgAddEdit_comboType.setSelectedIndex(0);
    }

    private Flight getFlightFromInputs() throws InvalidDataException {
        try {
            int id = Integer.parseInt("-".equals(dlgAddEdit_lblId.getText()) ? "0" : dlgAddEdit_lblId.getText());  // throw NumberFormatException
            Date onDay = Flight.dateFormat.parse(dlgAddEdit_tfDate.getText()); // throw ParseException
            String fromCode = dlgAddEdit_tfFromCode.getText();
            String toCode = dlgAddEdit_tfToCode.getText();
            Flight.Type type = (Flight.Type) dlgAddEdit_comboType.getSelectedItem();
            int passengers = (Integer) dlgAddEdit_sliderPassengers.getValue();
            Flight flight = new Flight(id, onDay, fromCode, toCode, type, passengers); // throw InvalidDataException
            return flight;
        } catch (ParseException | NumberFormatException ex) {
            throw new InvalidDataException(ex.getMessage());
        }

    }

    private void miAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_miAddMouseClicked
        cleanInputs();
        // show the dialog
        dlgAddEdit.pack();
        dlgAddEdit.setLocationRelativeTo(this);
        dlgAddEdit.setVisible(true);

    }//GEN-LAST:event_miAddMouseClicked

    private void dlgAddEdit_btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btSaveActionPerformed
        try {
            Flight flight = getFlightFromInputs(); // throw InvalidDataException
            if (currentlyEditedItemIndex == -1) {
                db.addFlight(flight); // throw SQLException
                loadFlightsFromDatabase();
            } else {
                db.updateFlight(flight); // throw SQLException
                loadFlightsFromDatabase();
            }
            dlgAddEdit.setVisible(false);
        } catch (SQLException | InvalidDataException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed add record " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgAddEdit_btSaveActionPerformed

    private void dlgAddEdit_btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btCancelActionPerformed
        dlgAddEdit.setVisible(false);
    }//GEN-LAST:event_dlgAddEdit_btCancelActionPerformed
    int currentlyEditedItemIndex = -1;
    private void lstFlightsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstFlightsMouseClicked
        if (evt.getClickCount() == 2 && evt.getButton() == MouseEvent.BUTTON1) {
            int index = lstFlights.getSelectedIndex();
            if (index == -1) {
                return;
            }
            currentlyEditedItemIndex = index;
            dlgAddEdit_lblId.setText(modelFlightsList.get(index).getId() + "");
            dlgAddEdit_tfDate.setText(Flight.dateFormat.format(modelFlightsList.get(index).getOnDay()));
            dlgAddEdit_tfFromCode.setText(modelFlightsList.get(index).getFromCode());
            dlgAddEdit_tfToCode.setText(modelFlightsList.get(index).getToCode());
            dlgAddEdit_sliderPassengers.setValue(modelFlightsList.get(index).getPassengers());
            modelTypeComboList.setSelectedItem((Flight.Type) modelFlightsList.get(index).getType());
            // show the dialog
            dlgAddEdit.pack();
            dlgAddEdit.setLocationRelativeTo(this);
            dlgAddEdit.setVisible(true);
        }
    }//GEN-LAST:event_lstFlightsMouseClicked

    private void dlgAddEdit_sliderPassengersStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_dlgAddEdit_sliderPassengersStateChanged
        dlgAddEdit_lblPassengers.setText(dlgAddEdit_sliderPassengers.getValue() + "");
    }//GEN-LAST:event_dlgAddEdit_sliderPassengersStateChanged

    private void popupDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_popupDeleteActionPerformed
        Flight flight = lstFlights.getSelectedValue();
        if (flight == null) {
            return;
        }
        int decision = JOptionPane.showConfirmDialog(this,
                "Are you sure you want to delete this item?\n" + flight,
                "Confirm deletion",
                JOptionPane.OK_CANCEL_OPTION);
        if (decision == JOptionPane.OK_OPTION) {
            try {
                db.deleteFlight(flight); // throw SQLException
                loadFlightsFromDatabase();
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Failed to delete record " + ex.getMessage(),
                        "Database error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_popupDeleteActionPerformed

    private void miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExitActionPerformed
        dispose();
    }//GEN-LAST:event_miExitActionPerformed

    private void miSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSaveActionPerformed
        fileChooser.setDialogTitle("Export to file");
        int returnVal = fileChooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file == null) {
                return;
            }
            // if (!file.getName().toLowerCase().endsWith(".txt")) {
            if (!file.getName().matches(".+\\.[a-zA-Z0-9]+")) {
                file = new File(file.getParentFile(), file.getName() + ".txt");
                System.out.println("Saving: " + file);
            }
            saveDataToFile(file);
        }
    }//GEN-LAST:event_miSaveActionPerformed

    private void saveDataToFile(File file) {
        try ( PrintWriter fileOutput = new PrintWriter(new FileWriter(file, false))) {
            for (Flight f : lstFlights.getSelectedValuesList()) {
                fileOutput.println(f.toDataString());
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Unable to write file:\n" + ex.getMessage(),
                    "Fatal error - file access error:", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day10flights.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day10flights.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day10flights.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day10flights.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day10flights().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JDialog dlgAddEdit;
    private javax.swing.JButton dlgAddEdit_btCancel;
    private javax.swing.JButton dlgAddEdit_btSave;
    private javax.swing.JComboBox<Flight.Type> dlgAddEdit_comboType;
    private javax.swing.JLabel dlgAddEdit_lblId;
    private javax.swing.JLabel dlgAddEdit_lblPassengers;
    private javax.swing.JSlider dlgAddEdit_sliderPassengers;
    private javax.swing.JFormattedTextField dlgAddEdit_tfDate;
    private javax.swing.JTextField dlgAddEdit_tfFromCode;
    private javax.swing.JTextField dlgAddEdit_tfToCode;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JList<Flight> lstFlights;
    private javax.swing.JMenu miAdd;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JMenuItem popupDelete;
    private javax.swing.JPopupMenu popupListFlights;
    // End of variables declaration//GEN-END:variables
}
