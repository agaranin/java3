package day10flights;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author artem
 */
public class Database {

    Connection conn;

    public Database() throws SQLException {
        String dbURL = "jdbc:sqlserver://localhost:1433;"
                + "database=FlightsDB;"
                + "user=sa;"
                + "password=7nZp6s240686;";
        conn = DriverManager.getConnection(dbURL); // throws SQLException
    }

    public ArrayList<Flight> getAllFlights() throws SQLException, InvalidDataException {
        ArrayList<Flight> list = new ArrayList<>();
        String sql = "SELECT * FROM dbo.Flights";

        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql); // throws SQLException

        while (result.next()) {
            int id = result.getInt("id");
            Date onDate = result.getDate("onDay");
            String fromCode = result.getString("fromCode");
            String toCode = result.getString("toCode");
            String typeStr = result.getString("type");
            Flight.Type type = Flight.Type.valueOf(typeStr); // ex IllegalArgumentException
            int passengers = result.getInt("passengers");
            list.add(new Flight(id, onDate, fromCode, toCode, type, passengers)); //InvalidDataException
        }
        return list;
    }

    public int addFlight(Flight f) throws SQLException {
        String sql = "INSERT INTO dbo.Flights (onDay, fromCode, toCode, type, passengers) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setDate(1, new java.sql.Date(f.getOnDay().getTime()));
        statement.setString(2, f.getFromCode());
        statement.setString(3, f.getToCode());
        statement.setString(4, f.getType().name());
        statement.setInt(5, f.getPassengers());

        statement.executeUpdate(); // throws SQLException
        ResultSet rs = statement.getGeneratedKeys(); // throws SQLException
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }
        // return -1;
        throw new SQLException("Id after insert not found");
    }

    public void updateFlight(Flight f) throws SQLException {
        String sql = "UPDATE dbo.Flights SET onDay=?, fromCode=?, toCode=?, type=?, passengers=? WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setDate(1, new java.sql.Date(f.getOnDay().getTime()));
        statement.setString(2, f.getFromCode());
        statement.setString(3, f.getToCode());
        statement.setString(4, f.getType().name());
        statement.setInt(5, f.getPassengers());
        statement.setInt(6, f.getId());
        statement.executeUpdate(); // throws SQLException
    }

    public void deleteFlight(Flight f) throws SQLException {
        String sql = "DELETE FROM dbo.Flights WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, f.getId());
        statement.executeUpdate(); // throws SQLException
    }

}
