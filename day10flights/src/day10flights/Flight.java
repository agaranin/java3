
package day10flights;

import java.text.SimpleDateFormat;
import java.util.Date;

class InvalidDataException extends Exception {
	public InvalidDataException(String msg, Throwable cause) {
		super(msg, cause);
	}
	public InvalidDataException(String msg) {
		super(msg);
	}
	public InvalidDataException() {
	}
}

public class Flight {
    private int id;
    private Date onDay;
    private String fromCode;
    private String toCode;
    private Type type;
    private int passengers;

    public Flight(int id, Date onDay, String fromCode, String toCode, Type type, int passengers) throws InvalidDataException {
        this.id = id;
        this.onDay = onDay;
        setFromCode(fromCode);
        setToCode(toCode);
        this.type = type;
        this.passengers = passengers;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOnDay() {
        return onDay;
    }

    public void setOnDay(Date onDay) {
        this.onDay = onDay;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(String fromCode) throws InvalidDataException {
        if (!fromCode.matches("[A-Z]{3,5}")) {
            throw new InvalidDataException("Code must be 3-5 uppercase letters: " + fromCode);
        }
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(String toCode) throws InvalidDataException {
        if (!toCode.matches("[A-Z]{3,5}")) {
            throw new InvalidDataException("Code must be 3-5 uppercase letters: " + toCode);
        }
        this.toCode = toCode;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }
    
    public enum Type {
        Domestic, International, Private
    }
    
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    public String toString() {
        return String.format("%s flight, %d passengers, on %s, from %s to %s", type, passengers, dateFormat.format(onDay), fromCode, toCode );
    }
    public String toDataString() {
        return String.format("%s,%d,%s,%s,%s", type, passengers, dateFormat.format(onDay), fromCode, toCode );
    }
}
