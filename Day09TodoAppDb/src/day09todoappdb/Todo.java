
package day09todoappdb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author artem
 */
public class Todo {

        private int id;
        private String task; // 1-100 characters (matching varchar length!), any characters are allowed
	private int difficulty; // 1-5, as slider
	private Date dueDate; // year 1900-2100 both inclusive
	private Status status; // one of: Pending, Done, Delegated - matches the ComboBox in Swing GUI

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) throws InvalidDataException {
        if (!task.matches(".{1,100}")) {
            throw new InvalidDataException("Task must be 1-100 characters" + task);
        }
        this.task = task;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) throws InvalidDataException {
        if (difficulty < 1 || difficulty > 5) {
            throw new InvalidDataException("difficulty must have 1-5: " + difficulty);
        }
        this.difficulty = difficulty;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) throws InvalidDataException {
        try {
            if (dueDate.before(dateFormat.parse("1970-01-01")) || dueDate.after(dateFormat.parse("2100-01-01"))) {
                throw new InvalidDataException("Year must be between 1970 and 2100: " + dateFormat.format(dueDate));
            }
        } catch (ParseException ex) {
            throw new InvalidDataException(ex.getMessage());
        }
        this.dueDate = dueDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
        
    enum Status {
    Pending, Done, Delegated
    }

    public Todo(int id, String task, int difficulty, Date dueDate, Status status) throws InvalidDataException {
        setId(id);
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate);
        setStatus(status);
    }
    
    
    
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public String toString() {
        return String.format(" %s by %s, difficulty: %d, status: %s", task, dateFormat.format(dueDate), difficulty, status);
    }
    
}
