package day09todoappdb;

import day09todoappdb.Todo.Status;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

public class Database {

    Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/ipd23todo", "root", "7nZp6s240686");
    }

    public ArrayList<Todo> getAllTodos() throws SQLException, InvalidDataException {
        ArrayList<Todo> list = new ArrayList<>();
        String sql = "SELECT * FROM todos";

        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql);

        while (result.next()) {
            int id = result.getInt("id");
            String task = result.getString("task");
            int difficulty = result.getInt("difficulty");
            Date dueDate = result.getDate("dueDate");
            Status status = Status.valueOf(result.getString("Status"));
            
            list.add(new Todo(id, task, difficulty, dueDate, status));
        }
        return list;
    }

    public int addTodo(Todo t) throws SQLException {
        String sql = "INSERT INTO todos (task, difficulty, dueDate, status) VALUES (?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, t.getTask());
        statement.setInt(2, t.getDifficulty());
        statement.setDate(3, new java.sql.Date(t.getDueDate().getTime()));
        statement.setString(4, t.getStatus().name());

        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }
        // return -1;
        throw new SQLException("Id after insert not found");
    }

    public void updateTodo(Todo t) throws SQLException {
        String sql = "UPDATE todos SET task=?, difficulty=?, dueDate=?, status=? WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, t.getTask());
        statement.setInt(2, t.getDifficulty());
        statement.setDate(3, new java.sql.Date(t.getDueDate().getTime()));
        statement.setString(4, t.getStatus().name());
        statement.setInt(5, t.getId());
        statement.executeUpdate();
    }

    public void deleteTodo(Todo t) throws SQLException {
        String sql = "DELETE FROM todos WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, t.getId());
        statement.executeUpdate();
    }

}
