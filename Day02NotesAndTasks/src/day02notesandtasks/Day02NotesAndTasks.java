/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day02notesandtasks;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author artem
 */
public class Day02NotesAndTasks {
    public static void main(String[] args){
        FileWriter fw = null;
        try {
            Random rn = new Random();
            int count = rn.nextInt(10) + 1;
            String name;
            System.out.println("Enter your name: ");
            name = getString();
//            System.out.println("your name: " + name);
            File file = new File("ouput.txt");
            if(!file.exists()){
                file.createNewFile();
            }
            fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            for (int i = 0; i < count; i++) {
                pw.append(String.format("%n%s;",name));
            }
            pw.close();
//            Part 2
                Scanner inFile = new Scanner (new FileReader ("ouput.txt"));
                ArrayList<String> aName = new ArrayList<String>(); 
                int i = 0;
                while (inFile.hasNextLine()) {
                aName.add(i, inFile.nextLine());
                i++;
                }
                System.out.println("Read from file:");
                for(String elem : aName){
                System.out.println(elem);
                }
                inFile.close();
                           
        } catch (IOException ex) {
            System.out.println("Cannot write the file" + ex.getMessage());
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                System.out.println("Cannot write the file" + ex.getMessage());
            }
        }
    }
    
public static String getString(){
    Scanner input = new Scanner(System.in);
    String value = input.nextLine();              
    return value;
  } //end method getString
}
