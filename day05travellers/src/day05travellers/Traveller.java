
package day05travellers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author artem
 */
public class Traveller {

    public Traveller(String name, Gender gender, String passportNo, String destAirportCode, Date depDate, Date retDate) throws InvalidDataException {
        setName(name);
        setGender(gender);
        setPassportNo(passportNo);
        setDestAirportCode(destAirportCode);
        setDepRetDates(depDate, retDate);

    }

    public Traveller(String dataLine) throws InvalidDataException {
        try {
            String[] splittedData = dataLine.split(";");
            if (splittedData.length != 6) {
                throw new InvalidDataException("Invalid data structure ");
            }
            setName(splittedData[0]);
            setGender(Gender.valueOf(splittedData[1]));
            setPassportNo(splittedData[2]);
            setDestAirportCode(splittedData[3]);
            setDepRetDates(dateFormat.parse(splittedData[4]), dateFormat.parse(splittedData[5]));
        } catch (ParseException ex) {
            throw new InvalidDataException(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            throw new InvalidDataException(ex.getMessage());
        }

    }

    private String name; // 2-50 characters, uppercase/lowercase, digits, .()-
    private Gender gender;
    private String passportNo; // AA123456 - two uppercase letters, then 6 digits
    private String destAirportCode; // three uppercase letters, e.g. YUL
    private Date depDate; // departure and return date, retDate must be after depDate
    private Date retDate; // departure and return date, retDate must be after depDate
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidDataException {
        if (!name.matches("[-()\\.A-Za-z0-9]{2,50}")) {
            throw new InvalidDataException("Name must be 2-50 characters, uppercase/lowercase, digits, .()-: " + name);
        }
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }
    
    public String getGenderStr() {
        return gender.toString();
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) throws InvalidDataException {
        if (!passportNo.matches("[A-Z][A-Z][0-9]{6}")) {
            throw new InvalidDataException("Passport Number must be two uppercase letters, then 6 digits: " + passportNo);
        }
        this.passportNo = passportNo;
    }

    public String getDestAirportCode() {
        return destAirportCode;
    }

    public void setDestAirportCode(String destAirportCode) throws InvalidDataException {
        if (!destAirportCode.matches("[A-Z]{3}")) {
            throw new InvalidDataException("Airport Code must be three uppercase letters, e.g. YUL: " + destAirportCode);
        }
        this.destAirportCode = destAirportCode;
    }

    public String getDepDate() {
        return dateFormat.format(depDate);
    }
    public Long getDepDateLong() {
        return depDate.getTime();
    }

    public void setDepRetDates(Date depDate, Date retDate) throws InvalidDataException {
        if (!retDate.after(depDate)) {
            throw new InvalidDataException("Return date " + dateFormat.format(retDate)
                    + " must be after Departure date " + dateFormat.format(depDate));
        }
        this.depDate = depDate;
        this.retDate = retDate;
    }

    public String getRetDate() {
        return dateFormat.format(retDate);
    }
    public Long getTripLength() {
        return retDate.getTime() - depDate.getTime();
    }
  

    @Override
    public String toString() {
        return String.format("Traveller name: %s, %s, Passport: %s, Airport: %s, Departure date: %s, Return date: %s",
                getName(), getGender(), getPassportNo(), getDestAirportCode(), getDepDate(), getRetDate());
    }

    public String toDateString() {
        return String.format("%s;%s;%s;%s;%s;%s", getName(), getGender(), getPassportNo(), getDestAirportCode(), getDepDate(), getRetDate());
    }

}
