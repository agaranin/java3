package day09peopledb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {

    Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/ipd23people", "root", "7nZp6s240686");
    }

    public ArrayList<Person> getAllPeople() throws SQLException {
        ArrayList<Person> list = new ArrayList<>();
        String sql = "SELECT * FROM people";

        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql);

        while (result.next()) {
            int id = result.getInt("id");
            String name = result.getString("name");
            int age = result.getInt("age");
//                    System.out.printf("Person: id=%d, name=%s, age=%d\n", id, name, age);

            list.add(new Person(id, name, age));
        }
        return list;
    }

    public int addPerson(Person p) throws SQLException {
        String sql = "INSERT INTO people (name, age) VALUES (?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, p.name);
        statement.setInt(2, p.age);

        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }
        // return -1;
        throw new SQLException("Id after insert not found");
    }

    public void updatePerson(Person p) throws SQLException {
        String sql = "UPDATE people SET name=?, age=? WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, p.name);
        statement.setInt(2, p.age);
        statement.setInt(3, p.id);
        statement.executeUpdate();
    }

    public void deletePerson(int id) throws SQLException {
        String sql = "DELETE FROM people WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();
    }

}
