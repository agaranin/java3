package day03peopleadvanceed;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

class Person implements Serializable {
    //default serialVersion id
    private static final long serialVersionUID = 1L;

    public Person(String name, int heightCm, Date dateOfBirth) {
        this.name = name;
        this.heightCm = heightCm;
        this.dateOfBirth = dateOfBirth;
    }
        
        
	String name;
	int heightCm;
	Date dateOfBirth;
	
	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	
	@Override
	public String toString() {
		// TODO: Fix display of the date
		return String.format("%s is %dcm tall born %s", name, heightCm, dateFormat.format(dateOfBirth));
//                return String.format("%s is %dcm tall born %s", name, heightCm, dateOfBirth);
	}
}