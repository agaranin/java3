/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09firstsql;

import java.util.Scanner;
import java.sql.*;

/**
 *
 * @author artem
 */
public class Day09FirstSQL {

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ipd23people", "root", "7nZp6s240686");
            // Insert
            {
                System.out.println("Enter name: ");
                String name = input.nextLine();
                System.out.println("Enter age: ");
                int age = input.nextInt();

                String sql = "INSERT INTO people (name, age) VALUES (?, ?)";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, name);
                statement.setInt(2, age);

                int rowsInserted = statement.executeUpdate();
                if (rowsInserted > 0) {
                    System.out.println("A new person record was inserted succesfully!");
                }
            }
                // Select
            {
                String sql = "SELECT * FROM people";

                Statement statement = conn.createStatement();
                ResultSet result = statement.executeQuery(sql);

                while (result.next()) {
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    System.out.printf("Person: id=%d, name=%s, age=%d\n", id, name, age);
                }

            }
            // Update
            {
                System.out.println("Enter id of the record to update: ");
                int id = input.nextInt();
                input.nextLine();
                System.out.println("New name: ");
                String name = input.nextLine();
                System.out.println("New age: ");
                int age = input.nextInt();
                input.nextLine();

                String sql = "UPDATE people SET name=?, age=? WHERE id=?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setString(1, name);
                statement.setInt(2, age);
                statement.setInt(3, id);

                statement.executeUpdate();
                System.out.println("Person updated successfully!");

            }
            // Delete
            {
                System.out.println("Enter id of the record to update: ");
                int id = input.nextInt();
                input.nextLine();
                
                String sql = "DELETE FROM people WHERE id=?";
                PreparedStatement statement = conn.prepareStatement(sql);
                statement.setInt(1, id);

                int rowsDeleted = statement.executeUpdate();
                if (rowsDeleted > 0) {
                    System.out.println("Record deleted successfully!");
                } else {
                    System.out.println("Not such record found - none deleted");
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

}
