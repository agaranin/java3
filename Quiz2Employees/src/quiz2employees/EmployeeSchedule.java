package quiz2employees;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author artem
 */
public class EmployeeSchedule {
//  EmployeeSchedule(String name, boolean isManager, String department, Date dateHired) { ... }
//  EmployeeSchedule(String dataLine) { ... }
  private String name; // 2-50 characters, not permitted are: ;^?@!~*
  private boolean isManager;
  private String department; // 2-50 characters, not permitted are: ;^?@!~*
  private Date dateHired; // year between 1900 and 2100
  private HashSet<Weekday> workdaysList = new HashSet<>(); // no duplicates allowed

    public EmployeeSchedule(String name, boolean isManager, String department, Date dateHired, HashSet<Weekday> workdaysList) throws InvalidDataException {
        setName(name);
        setIsManager(isManager);
        setDepartment(department);
        setDateHired(dateHired);
        setWorkdaysList(workdaysList);
    }
    
    public EmployeeSchedule(String dataLine) throws InvalidDataException {
        try {
            String[] splittedData = dataLine.split(";");
            if (splittedData.length != 4) {
                throw new InvalidDataException("Invalid data structure ");
            }
            if(splittedData[0].contains("*"))
            setIsManager(true);
            if(splittedData[0].contains("*")){
//            splittedData[0].replace("*", "");
            setName(splittedData[0]);
            }
            setDateHired(dateFormat.parse(splittedData[1]));
            setDepartment(splittedData[2]);
            String[] splittedWorkDays = splittedData[3].split(",");
            for(String str: splittedWorkDays){
            workdaysList.add(Weekday.valueOf(str.trim()));
            }
            
        } catch (ParseException ex) {
            throw new InvalidDataException(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            throw new InvalidDataException(ex.getMessage());
        }
    }

    public HashSet<Weekday> getWorkdaysList() {
        return workdaysList;
    }

    public void setWorkdaysList(HashSet<Weekday> workdaysList) {
        this.workdaysList = workdaysList;
    }

    public String getName() {
        return name;
    }
    public String getManagerName() {
        if(isManager)
        return name.concat("*");
        else
        return name;
    }

    public void setName(String name) throws InvalidDataException {
       if (!name.matches("[^;?@!~*^]{2,50}")) {
            throw new InvalidDataException("Name must be 2-50 characters, not permitted are: ;^?@!~*" + name);
        }
        this.name = name;
    }

    public boolean isIsManager() {
        return isManager;
    }

    public void setIsManager(boolean isManager) {
        this.isManager = isManager;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getDateHired() {
        return dateHired;
    }

    public void setDateHired(Date dateHired) throws InvalidDataException {
        try {
            if (dateHired.before(dateFormat.parse("1900-01-01")) || dateHired.after(dateFormat.parse("2100-01-01"))) {
                throw new InvalidDataException("Year must be between 1900 and 2100" + dateHired);
            }
        } catch (ParseException ex) {
            throw new InvalidDataException(ex.getMessage());
        }
        this.dateHired = dateHired;
    }

    
  static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  
    @Override
    public String toString() {
        if(isManager)
        return String.format("%s, manager of %s, hired %s work on %s", name, department, dateFormat.format(dateHired), getWorkdaysList().toString());   
        else
        return String.format("%s, %s, hired %s work on %s", name, department, dateFormat.format(dateHired), getWorkdaysList().toString());
    }

    public String toDateString() {
        return String.format("%s;%s;%s;%s", getManagerName(), dateFormat.format(dateHired), department, getWorkdaysList().toString());
    }
  
}
