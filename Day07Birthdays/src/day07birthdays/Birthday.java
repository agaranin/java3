package day07birthdays;

import com.opencsv.bean.CsvBindByPosition;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.time.temporal.ChronoUnit;

/**
 *
 * @author artem
 */
public class Birthday {

    @CsvBindByPosition(position = 0)
    private String name;
    @CsvBindByPosition(position = 1)
    private Date bithday;

    public Birthday(String name, Date bithday) throws InvalidDataException {
        setName(name);
        setBithday(bithday);
    }

    public Birthday(String dataLine) throws InvalidDataException {
        try {
            String[] splittedData = dataLine.split(";");
            if (splittedData.length != 2) {
                throw new InvalidDataException("Invalid data structure ");
            }
            setName(splittedData[0]);
            setBithday(dateFormat.parse(splittedData[1]));
        } catch (ParseException | IllegalArgumentException ex) {
            throw new InvalidDataException(ex.getMessage());
        }

    }

    public int getDaysTillBday() {
        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.ofEpochDay((long) (bithday.getTime() / 8.64e+7));
        LocalDate nextBDay = birthday.withYear(today.getYear());
        //If your birthday has occurred this year already, add 1 to the year.
        if (nextBDay.isBefore(today) || nextBDay.isEqual(today)) {
            nextBDay = nextBDay.plusYears(1);
        }
        long p = ChronoUnit.DAYS.between(today, nextBDay);
        return (int) p;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidDataException {
        if (!name.matches("[^;]{1,50}")) {
            throw new InvalidDataException("Name must be 1-50, no semicolon" + name);
        }
        this.name = name;
    }

    public Date getBithday() {
        return bithday;
    }

    public void setBithday(Date bithday) {
        this.bithday = bithday;
    }
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public String toString() {
        return String.format("%s born %s birthday in %d days", name, dateFormat.format(bithday), getDaysTillBday());
    }

    public String toDateString() {
        return String.format("%s;%s", name, dateFormat.format(bithday));
    }

}
