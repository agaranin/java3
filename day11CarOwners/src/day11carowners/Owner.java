package day11carowners;

/**
 *
 * @author artem
 */
public class Owner {

    private int id;
    private String name;
    private byte[] photo;
    private int carCount;

    public Owner(int id, String name, byte[] photo, int carCount) {
        setId(id);
        setName(name);
        setPhoto(photo);
        setCarCount(carCount);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public int getCarCount() {
        return carCount;
    }

    public void setCarCount(int carCount) {
        this.carCount = carCount;
    }

    @Override
    public String toString() {
        if (getCarCount() > 1) {
            return String.format("%d: %s own %d cars", id, name, getCarCount());
        } else {
            return String.format("%d: %s own %d car", id, name, getCarCount());
        }
    }
}
