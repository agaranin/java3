
package day11carowners;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class OwnerRenderer extends JLabel implements ListCellRenderer<Owner> {
public OwnerRenderer() {
         setOpaque(true);
     }
    @Override
    public Component getListCellRendererComponent(JList<? extends Owner> list, Owner owner, int index,
        boolean isSelected, boolean cellHasFocus) {
        Color background;
        Color foreground;
        setText(owner.getId() + ": " + owner.getName());
        if (isSelected) {
             background = Color.DARK_GRAY;
             foreground = Color.WHITE;

         } else {
             background = Color.WHITE;
             foreground = Color.BLACK;
         }
         setBackground(background);
         setForeground(foreground);
        
        return this;
    }
     
}