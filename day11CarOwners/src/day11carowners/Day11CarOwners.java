/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author artem
 */
public class Day11CarOwners extends javax.swing.JFrame {

    Database db;
    BufferedImage currentImage = null;
    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    DefaultListModel<Owner> modelOwnersList = new DefaultListModel<>();
    DefaultListModel<Car> modelCarsList = new DefaultListModel<>();
    DefaultListModel<Car> modelDlgCarsList = new DefaultListModel<>();
    DefaultListModel<Owner> modelDlgOwnersList = new DefaultListModel<>();

    /**
     * Creates new form Day11CarOwners
     */
    public Day11CarOwners() {
        initComponents();
        try {
            db = new Database(); // throw SQLException
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to connect" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        loadOwnersFromDatabase();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Image file (*.jpg, *.jpeg, *.png, *.gif)", "jpg", "jpeg", "png", "gif"));
    }
    
    void loadOwnersFromDatabase() {
        try {
            ArrayList<Owner> list = db.getAllOwners(); // throw SQLException, InvalidDataException
            for (Owner owner: list ){
            owner.setCarCount(db.getCarCountByOwnerId(owner.getId()));
            }
            modelOwnersList.clear();
            modelOwnersList.addAll(list);
            modelDlgOwnersList.clear();
            modelDlgOwnersList.addAll(list);
            dlgCars_lstOwners.setCellRenderer(new OwnerRenderer());
        } catch (SQLException | InvalidDataException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to get all todos" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }     
    }
    
    void loadCarsFromDatabase() {
        try {
            ArrayList<Car> list = db.getAllCars(); // throw SQLException, InvalidDataException
            for (Car car: list){
            car.setOwnerName(db.getOwnerNameById(car.getOwnerId()));
            }
            modelDlgCarsList.clear();
            modelDlgCarsList.addAll(list);
        } catch (SQLException | InvalidDataException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to get all cars" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }     
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgCars = new javax.swing.JDialog();
        jScrollPane3 = new javax.swing.JScrollPane();
        dlgCars_lstCars = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        dlgCars_lblId = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        dlgCars_tfMakeModel = new javax.swing.JTextField();
        dlgCars_tfProdYear = new javax.swing.JTextField();
        dlgCars_tfPlates = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        dlgCars_lstOwners = new javax.swing.JList<>();
        dlgCars_btUpdateOwner = new javax.swing.JButton();
        dlgCars_btAdd = new javax.swing.JButton();
        dlgCars_btDelete = new javax.swing.JButton();
        dlgCars_btUpdate = new javax.swing.JButton();
        fileChooser = new javax.swing.JFileChooser();
        btManageCars = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblId = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        lblPhoto = new javax.swing.JLabel();
        btDeletePhoto = new javax.swing.JButton();
        btAddOwner = new javax.swing.JButton();
        btUpdateOwner = new javax.swing.JButton();
        btDeleteOwner = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstCarOwnes = new javax.swing.JList<>();
        btGiveUpCar = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        lstOwners = new javax.swing.JList<>();

        dlgCars.setModal(true);

        dlgCars_lstCars.setModel(modelDlgCarsList);
        jScrollPane3.setViewportView(dlgCars_lstCars);

        jLabel2.setText("Id:");

        dlgCars_lblId.setText("-");

        jLabel7.setText("Make Model:");

        jLabel8.setText("Prod. Year:");

        jLabel9.setText("Plates:");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setText("Has Owner:");

        dlgCars_lstOwners.setModel(modelDlgOwnersList);
        jScrollPane4.setViewportView(dlgCars_lstOwners);

        dlgCars_btUpdateOwner.setText("Update Owner");
        dlgCars_btUpdateOwner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCars_btUpdateOwnerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel10))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(dlgCars_btUpdateOwner)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dlgCars_btUpdateOwner)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        dlgCars_btAdd.setText("Add");
        dlgCars_btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCars_btAddActionPerformed(evt);
            }
        });

        dlgCars_btDelete.setText("Delete");

        dlgCars_btUpdate.setText("Update");

        javax.swing.GroupLayout dlgCarsLayout = new javax.swing.GroupLayout(dlgCars.getContentPane());
        dlgCars.getContentPane().setLayout(dlgCarsLayout);
        dlgCarsLayout.setHorizontalGroup(
            dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgCarsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgCarsLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dlgCars_tfMakeModel, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
                            .addComponent(dlgCars_tfProdYear)
                            .addComponent(dlgCars_tfPlates)))
                    .addGroup(dlgCarsLayout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dlgCars_lblId))
                    .addGroup(dlgCarsLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dlgCars_btAdd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgCars_btDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgCars_btUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        dlgCarsLayout.setVerticalGroup(
            dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgCarsLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(dlgCarsLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgCarsLayout.createSequentialGroup()
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(dlgCars_lblId))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(dlgCars_tfMakeModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(dlgCars_tfProdYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(dlgCarsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(dlgCars_tfPlates, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(dlgCars_btAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgCars_btDelete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgCars_btUpdate)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        btManageCars.setText("Manage Cars");
        btManageCars.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btManageCarsActionPerformed(evt);
            }
        });

        jLabel1.setText("Id:");

        lblId.setText("-");

        jLabel3.setText("Name:");

        jLabel4.setText("Photo:");

        lblPhoto.setText("Choose photo");
        lblPhoto.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblPhoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPhotoMouseClicked(evt);
            }
        });

        btDeletePhoto.setText("Del.Photo");

        btAddOwner.setText("Add");
        btAddOwner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddOwnerActionPerformed(evt);
            }
        });

        btUpdateOwner.setText("Update");

        btDeleteOwner.setText("Delete");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setText("Car owned:");

        lstCarOwnes.setModel(modelCarsList);
        jScrollPane2.setViewportView(lstCarOwnes);

        btGiveUpCar.setText("Give up Car");
        btGiveUpCar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGiveUpCarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(btGiveUpCar)
                .addContainerGap(81, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btGiveUpCar)
                .addContainerGap())
        );

        lstOwners.setModel(modelOwnersList);
        lstOwners.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstOwnersValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(lstOwners);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btManageCars)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btAddOwner))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btUpdateOwner)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDeleteOwner))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btDeletePhoto)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(16, 16, 16)
                                    .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(tfName))))))
                .addGap(39, 39, 39)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(lblId))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(39, 39, 39)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDeletePhoto))
                    .addComponent(jScrollPane5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btAddOwner)
                    .addComponent(btManageCars)
                    .addComponent(btUpdateOwner)
                    .addComponent(btDeleteOwner))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btGiveUpCarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGiveUpCarActionPerformed
        try {
            Car car = lstCarOwnes.getSelectedValue();
            db.giveUpCar(car);
            Owner owner = lstOwners.getSelectedValue();
            modelCarsList.clear();
            modelCarsList.addAll(db.getCarsByOwnerId(owner.getId()));
           loadOwnersFromDatabase();
        } catch (SQLException | InvalidDataException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed give up car " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btGiveUpCarActionPerformed

    private void btManageCarsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btManageCarsActionPerformed
//        cleanInputs();
        loadCarsFromDatabase();
        // show the dialog
        dlgCars.pack();
        dlgCars.setLocationRelativeTo(this);
        dlgCars.setVisible(true);
    }//GEN-LAST:event_btManageCarsActionPerformed

    private void lblPhotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPhotoMouseClicked
        fileChooser.setDialogTitle("Open image");
        int returnVal = fileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            if (file == null) {
                return;
            }
            try {
                currentImage = ImageIO.read(file); // ex IOException
                Image scaledImg = currentImage.getScaledInstance(lblPhoto.getWidth(), lblPhoto.getHeight(), Image.SCALE_SMOOTH);
                lblPhoto.setIcon(new ImageIcon(scaledImg));
                ImageIO.write(currentImage, "jpg", bos); // ex IOException
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this, "Error loading image file: " + ex.getMessage(),
                        "File error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_lblPhotoMouseClicked

    private void btAddOwnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddOwnerActionPerformed
        try {
            byte[] photo = bos.toByteArray();
            String name = tfName.getText();
            db.addOwner(new Owner(0, name, photo, 0)); // ex SQLException
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed add record " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btAddOwnerActionPerformed

    private void dlgCars_btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCars_btAddActionPerformed
        try {
            int ownerId = 0;
            if (!dlgCars_lstOwners.isSelectionEmpty()) {
                ownerId = dlgCars_lstOwners.getSelectedValue().getId();
            }
            String makeModel = dlgCars_tfMakeModel.getText();
            int prodYear = Integer.parseInt(dlgCars_tfProdYear.getText());
            String plate = dlgCars_tfPlates.getText();
            db.addCar(new Car(0, ownerId, makeModel, prodYear, plate));
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed add record " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgCars_btAddActionPerformed

    private void lstOwnersValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstOwnersValueChanged
        try {
            Owner owner = lstOwners.getSelectedValue();
            lblId.setText(owner.getId()+"");
            tfName.setText(owner.getName());
            byte[] photo = db.getPhotoById(owner.getId());
            ByteArrayInputStream bis = new ByteArrayInputStream(photo);
            BufferedImage bufImg = ImageIO.read(bis);
            Image scaledImg = bufImg.getScaledInstance(lblPhoto.getWidth(), lblPhoto.getHeight(), Image.SCALE_SMOOTH);
            lblPhoto.setIcon(new ImageIcon(scaledImg));
            lstCarOwnes.setCellRenderer(new CarsRenderer());
            modelCarsList.clear();
            modelCarsList.addAll(db.getCarsByOwnerId(owner.getId()));
        } catch (SQLException | IOException | InvalidDataException ex) {
            JOptionPane.showMessageDialog(this,
                    "Failed get owner " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_lstOwnersValueChanged

    private void dlgCars_btUpdateOwnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCars_btUpdateOwnerActionPerformed
        try {
            Car car = dlgCars_lstCars.getSelectedValue();
            Owner owner = dlgCars_lstOwners.getSelectedValue();
            db.updateOwner(car, owner);   
           loadOwnersFromDatabase();
           loadCarsFromDatabase();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed give up car " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgCars_btUpdateOwnerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day11CarOwners.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day11CarOwners.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day11CarOwners.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day11CarOwners.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day11CarOwners().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddOwner;
    private javax.swing.JButton btDeleteOwner;
    private javax.swing.JButton btDeletePhoto;
    private javax.swing.JButton btGiveUpCar;
    private javax.swing.JButton btManageCars;
    private javax.swing.JButton btUpdateOwner;
    private javax.swing.JDialog dlgCars;
    private javax.swing.JButton dlgCars_btAdd;
    private javax.swing.JButton dlgCars_btDelete;
    private javax.swing.JButton dlgCars_btUpdate;
    private javax.swing.JButton dlgCars_btUpdateOwner;
    private javax.swing.JLabel dlgCars_lblId;
    private javax.swing.JList<Car> dlgCars_lstCars;
    private javax.swing.JList<Owner> dlgCars_lstOwners;
    private javax.swing.JTextField dlgCars_tfMakeModel;
    private javax.swing.JTextField dlgCars_tfPlates;
    private javax.swing.JTextField dlgCars_tfProdYear;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JList<Car> lstCarOwnes;
    private javax.swing.JList<Owner> lstOwners;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}
