package day11carowners;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author artem
 */
public class Database {

    Connection conn;

    public Database() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/carOwners", "root", "rootroot");
    }

    public int addOwner(Owner o) throws SQLException {
        String sql = "INSERT INTO owners (name, photo) VALUES (?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, o.getName());
        statement.setBytes(2, o.getPhoto());
        statement.executeUpdate(); // throws SQLException
        ResultSet rs = statement.getGeneratedKeys(); // throws SQLException
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }
        // return -1;
        throw new SQLException("Id after insert not found");
    }

    public int addCar(Car c) throws SQLException {
        String sql = "INSERT INTO cars (ownerId, makeModel, prodYear, plate) VALUES (?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setNull(1, java.sql.Types.NULL);
        if (c.getOwnerId() != 0) {
            statement.setInt(1, c.getOwnerId());
        }
        statement.setString(2, c.getMakeModel());
        statement.setInt(3, c.getProdYear());
        statement.setString(4, c.getPlate());
        statement.setNull(4, java.sql.Types.NULL);
        if (!c.getPlate().equals("")) {
            statement.setString(4, c.getPlate());
        }
        statement.executeUpdate(); // throws SQLException
        ResultSet rs = statement.getGeneratedKeys(); // throws SQLException
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }
        // return -1;
        throw new SQLException("Id after insert not found");
    }

    public ArrayList<Owner> getAllOwners() throws SQLException, InvalidDataException {
        ArrayList<Owner> list = new ArrayList<>();
        String sql = "SELECT id, name FROM owners";

        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql); // throws SQLException

        while (result.next()) {
            int id = result.getInt("id");
            String name = result.getString("name");
            byte[] photo = null;
            Owner owner = new Owner(id, name, photo, 0);
            list.add(owner); //InvalidDataException
        }
        return list;
    }

    public ArrayList<Car> getAllCars() throws SQLException, InvalidDataException {
        ArrayList<Car> list = new ArrayList<>();
        String sql = "SELECT * FROM cars";

        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql); // throws SQLException

        while (result.next()) {
            int id = result.getInt("id");
            int ownerId = result.getInt("ownerId");
            String makeModel = result.getString("makeModel");
            int prodYear = result.getInt("prodYear");
            String plate = result.getString("plate");
            Car car = new Car(id, ownerId, makeModel, prodYear, plate);
            list.add(car); //InvalidDataException
        }
        return list;
    }

    public String getOwnerNameById(int ownerId) throws SQLException {
        String resultStr = null;
        String sql = "SELECT O.name FROM cars as C inner join owners as O on C.ownerId = O.id WHERE C.ownerId = " + ownerId;
        PreparedStatement statement = conn.prepareStatement(sql);
//        statement.setInt(1, ownerId);
        ResultSet result = statement.executeQuery(sql); // throws SQLException
        while (result.next()) {
            resultStr = result.getString(1);
        }
        return resultStr;
    }

    public int getCarCountByOwnerId(int ownerId) throws SQLException {
        int resultStr = 0;
        String sql = "SELECT count(C.ownerId)\n"
                + "FROM cars as C join owners as O\n"
                + "on C.ownerId = O.id\n"
                + "WHERE C.ownerId = " + ownerId + "\n"
                + "GROUP by C.ownerId ";
        PreparedStatement statement = conn.prepareStatement(sql);
//        statement.setInt(1, ownerId);
        ResultSet result = statement.executeQuery(sql); // throws SQLException
        while (result.next()) {
            resultStr = result.getInt(1);
        }
        return resultStr;
    }
    
    public byte[] getPhotoById(int id) throws SQLException {
        byte[] photo = null;
        String sql = "SELECT photo FROM owners WHERE id = " + id;
        PreparedStatement statement = conn.prepareStatement(sql);
//        statement.setInt(1, id);
        ResultSet result = statement.executeQuery(sql); // throws SQLException
        while (result.next()) {
            photo = result.getBytes(1);
        }
        return photo;
    }
    
    public ArrayList<Car> getCarsByOwnerId(int ownerId) throws SQLException, InvalidDataException {
        ArrayList<Car> list = new ArrayList<>();
        String sql = "SELECT * FROM cars WHERE ownerId =" + ownerId;
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql); // throws SQLException

        while (result.next()) {
            int id = result.getInt("id");
            int ownerIdBd = result.getInt("ownerId");
            String makeModel = result.getString("makeModel");
            int prodYear = result.getInt("prodYear");
            String plate = result.getString("plate");
            Car car = new Car(id, ownerIdBd, makeModel, prodYear, plate);
            list.add(car); //InvalidDataException
        }
        return list;
    }
    
    public void giveUpCar(Car c) throws SQLException {
        String sql = "UPDATE cars SET ownerId=null WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, c.getId());
        statement.executeUpdate(); // throws SQLException
    }
    
    public void updateOwner(Car c, Owner o) throws SQLException {
        String sql = "UPDATE cars SET ownerId=? WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, o.getId());
        statement.setInt(2, c.getId());
        statement.executeUpdate(); // throws SQLException
    }
    

    /*
    public int addFlight(Flight f) throws SQLException {
        String sql = "INSERT INTO dbo.Flights (onDay, fromCode, toCode, type, passengers) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setDate(1, new java.sql.Date(f.getOnDay().getTime()));
        statement.setString(2, f.getFromCode());
        statement.setString(3, f.getToCode());
        statement.setString(4, f.getType().name());
        statement.setInt(5, f.getPassengers());

        statement.executeUpdate(); // throws SQLException
        ResultSet rs = statement.getGeneratedKeys(); // throws SQLException
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        }
        // return -1;
        throw new SQLException("Id after insert not found");
    }

    public void updateFlight(Flight f) throws SQLException {
        String sql = "UPDATE dbo.Flights SET onDay=?, fromCode=?, toCode=?, type=?, passengers=? WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setDate(1, new java.sql.Date(f.getOnDay().getTime()));
        statement.setString(2, f.getFromCode());
        statement.setString(3, f.getToCode());
        statement.setString(4, f.getType().name());
        statement.setInt(5, f.getPassengers());
        statement.setInt(6, f.getId());
        statement.executeUpdate(); // throws SQLException
    }

    public void deleteFlight(Flight f) throws SQLException {
        String sql = "DELETE FROM dbo.Flights WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, f.getId());
        statement.executeUpdate(); // throws SQLException
    }
     */
}
