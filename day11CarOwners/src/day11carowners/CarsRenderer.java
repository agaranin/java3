
package day11carowners;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class CarsRenderer extends JLabel implements ListCellRenderer<Car> {
public CarsRenderer() {
         setOpaque(true);
     }
    @Override
    public Component getListCellRendererComponent(JList<? extends Car> list, Car car, int index,
        boolean isSelected, boolean cellHasFocus) {
        Color background;
        Color foreground;
        setText(String.format("%d: %s, %d, %s", car.getId(), car.getMakeModel(), car.getProdYear(), car.getPlate()));
        if (isSelected) {
             background = Color.DARK_GRAY;
             foreground = Color.WHITE;

         } else {
             background = Color.WHITE;
             foreground = Color.BLACK;
         }
         setBackground(background);
         setForeground(foreground);
        
        return this;
    }
     
}
