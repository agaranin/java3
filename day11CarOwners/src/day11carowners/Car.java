/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author artem
 */
public class Car {

    private int id;
    private int ownerId;
    private String makeModel;
    private int prodYear;
    private String plate;
    private String ownerName;
    Database db;

    public Car(int id, int ownerId, String makeModel, int prodYear, String plate) {
        setId(id);
        setOwnerId(ownerId);
        setMakeModel(makeModel);
        setProdYear(prodYear);
        setPlate(plate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public int getProdYear() {
        return prodYear;
    }

    public void setProdYear(int prodYear) {
        this.prodYear = prodYear;
    }

    public String getPlate() {
        if (plate == null) {
            return "no plates";
        }
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }
     public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    
    public String getOwnerName() {
        if (ownerId == 0) {
            return "has no owner";
        } else {
        return "owner " + ownerName;
    }
    }

    @Override
    public String toString() {
        return String.format("%s, %d, %s, %s", makeModel, prodYear, getOwnerName(), getPlate());
    }

}
