drop database if exists carowners 
;
create database if not exists carOwners default character set utf8;
;
use carOwners
;
CREATE TABLE owners (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100),
  photo LONGBLOB NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX name_UNIQUE (name);

 
CREATE TABLE cars (
  id INT NOT NULL AUTO_INCREMENT,
  ownerId INT NULL,
  makeModel VARCHAR(100) NOT NULL,
  prodYear INT NOT NULL,
  plate VARCHAR(10) NULL,
  PRIMARY KEY (id),
  FOREIGN KEY(ownerId) REFERENCES owners(id),
  UNIQUE INDEX plates_UNIQUE (plates ASC) VISIBLE);