-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema carOwners
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema carOwners
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `carOwners` DEFAULT CHARACTER SET utf8 ;
USE `carOwners` ;

-- -----------------------------------------------------
-- Table `carOwners`.`owners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carOwners`.`owners` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `photo` LONGBLOB NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `carOwners`.`cars`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carOwners`.`cars` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ownerId` INT NULL DEFAULT NULL,
  `makeModel` VARCHAR(100) NOT NULL,
  `prodYear` INT NOT NULL,
  `plate` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `plates_UNIQUE` (`plate` ASC) VISIBLE,
  INDEX `ownerId` (`ownerId` ASC) VISIBLE,
  CONSTRAINT `cars_ibfk_1`
    FOREIGN KEY (`ownerId`)
    REFERENCES `carOwners`.`owners` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
