package day06teammembers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author artem
 */
public class Day06TeamMembers {
    
    public static void main(String[] args) {
        final String DATA_FILENAME = "teams.txt";
        HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();
        try ( Scanner fileInput = new Scanner(new File(DATA_FILENAME))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String splittedLine[] = line.split(":");
                String players[] = splittedLine[1].split(",");
                for (String player : players) {
                    if (playersByTeams.containsKey(player)) {
                        playersByTeams.get(player).add(splittedLine[0]);
                    } else {
                        // Version 1
                        playersByTeams.put(player, new ArrayList<>(Arrays.asList(splittedLine[0])));

                        // Version 2
//                        ArrayList<String> teams = new ArrayList<>();
//                        teams.add(splittedLine[0]);
//                        playersByTeams.put(player, teams);
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println("Errors reading data file: " + ex.getMessage());

        }
        playersByTeams.forEach((player, teams) -> {
            // Version 1
            String playerTeams = String.join(", ", teams);
            System.out.println(player + " plays in: " + playerTeams);

            // Version 2
//            System.out.println(player + " plays in: " + teams);
        });
    }
}
